[SIESTA](https://siesta-project.org/siesta) is a software for efficient electronic structure calculations and ab initio molecular dynamics simulations of molecules and solids.

This project contains the source for the top-level documentation for the SIESTA code itself.
It is intended to be a sub-project of 'siesta-project'.

It is inspired by the Divio Developer's Handbook (git@github.com:divio/divio-cloud-docs.git)

To build locally, you need sphinx and some extra modules.

With conda, you can install the requirements thus:

```
conda install sphinx
conda install -c foxbms sphinx_rtd_theme
```

(Note that you might be able to satisfy the above requirements through many other different means).

Then:

```
cd docs
make html
# open the _build/html/index.html file
```

(The previous setup copied from Divio has been streamlined to avoid too many dependencies)

  

