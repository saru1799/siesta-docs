import sisl , ase
from ase.neb import NEB

number_of_images = 7
interpolation_method = 'idpp'   # Try 'li' for linear interpolation

#Try For Unrelaxed Structures Uncomment #
#----------------------------
#FDF_initial = sisl.get_sile("<PATH TO FDF FILE *.fdf OF INITIAL STRUCTURE>")
#FDF_final = sisl.get_sile("<PATH TO FDF FILE *.fdf OF FINAL STRUCTURE")
#---------------------------------------------------------------------------

# If You Relaxed
FDF_initial = sisl.get_sile("<PATH TO XV FILE *.XV OF INITIAL STRUCTURE>")
FDF_final = sisl.get_sile("<PATH TO XV FILE *.XV OF FINAL STRUCTURE")

#===============================================================================

Geometry_initial = FDF_initial.read_geometry()
Geometry_final = FDF_final.read_geometry()
ASE_Geometry_initial = Geometry_initial.toASE()
ASE_Geometry_final = Geometry_final.toASE()

images = [ASE_Geometry_initial]
print ("Copying ASE For NEB Image  0")
for i in range(number_of_images):
    print ("Copying ASE For NEB Image ",i+1)
    images.append(ASE_Geometry_initial.copy())
images.append(ASE_Geometry_final)
print ("Copying ASE For NEB Image ",i+2)
neb = NEB(images)
neb.interpolate(interpolation_method)

for i in range(number_of_images+2):
    sisl_images = sisl.Geometry.fromASE(neb.images[i])
    sisl_images.write("image_{}.xyz".format(str(i)))
Geometry_initial.write("input.fdf")

