root=$(pwd)

for V in $(seq 0 0.1 1) ; do

   # Get directory name
   d=TBT_${V//,/.}

   echo Computing ${d}

   # Create the directory and enter
   mkdir ${root}/${d}
   cd ${root}/${d}

   # Copy all the input files from the 0V calculation
   cp ${root}/0V/*fdf ${root}/${d}
   # But overwrite the TBtrans.fdf with the input file containing
   # the options to compute only current.
   cp ${root}/TBtrans_current.fdf ${root}/${d}/TBtrans.fdf

   # Run TBtrans
   tbtrans -V "${V//,/.}:eV" RUN.fdf > TBT.out

done
