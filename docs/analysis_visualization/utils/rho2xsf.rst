:sequential_nav: next

..  _how-to-rho2xsf:

rho2xsf
=======

rho2xsf is a very basic tool to convert .RHO and .LDOS files into XSF format, to be later read by XCRYSDEN.
To use this tool you need and .XV file with your system's coordinates (usually a default output of SIESTA),
and a file with the property calculated on the grid, the most common being a .RHO file or a .LDOS file.
Note that all files should be in the same directory where you are executing rho2xsf.

.. note::
   rho2xsf does *not* work with NetCDF files (.nc).

.. note::
   In principle, it is possible to use rho2xsf with any kind of grid-based SIESTA
   output: .DRHO, .VH, .VT., etc. See the manual for more information on these files.


Input
-----
rho2xsf has a very straightforward interface; it will ask for the following inputs in order:

* System label.
* Whether the coordinates are in Angstrom (A) or Bohr (B).
* Origin of the coordinate system (three consecutive real numbers separated by spaces).
* First spanning (cell) vector (as above, three numbers separated by spaces).
* Second spanning vector (as above).
* Third spanning vector (as above).
* The number of grid points along each axis (three integers separated by spaces).

If an .XSF file is found in the current directory, it will ask whether you want to overwrite it.
Then you must specify as many input files as you need (RHO, DRHO, VH, VT, LDOS, etc.), and the
corresponding property will be written to the XSF file. For example:

* RHO: Print the charge density to the XSF file.
* LDOS: Print the LDOS to the XSF file.
* VH: Print the Hartree potential to the XSF file.

Once you are done, you can exit rho2xsf by typing BYE.

.. note::
   RHO and LDOS, and similar extensions *must* be written in the same case as the input file.
   For example, writing "ldos" in lower case (instead of LDOS, which is the file extension)
   will cause rho2xsf to fail.

Output
------

* "system label".XSF file, written on exit.


Providing an input file
-----------------------

You can pre-select all your choices for rho2xsf in an input file and then provide them to rho2xsf directly
via::

   rho2xsf < input_file

Here is an example of one such file::

   ch3
   A               # Use Angstrom
   -2.0 -2.0 -2.0  # Cell origin
    4.0  0.0  0.0  # spanning vectors
    0.0  4.0  0.0
    0.0  0.0  4.0
    40 40 40       # Number of grid points along each axis
   RHO
   LDOS
   BYE

.. note::
   When providing an input file, rho2xsf may fail if you already have an .XSF file within the working directory,
   unless you also provide the additional answer for overwriting the .XSF file.
