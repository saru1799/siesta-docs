SIESTA Dependencies
###################

.. _install_dependencies:

List of Dependencies
********************

Siesta heavily relies on numerous dependencies, some are required while some are
optional. And a few of each are automatically fetched by CMake at compilation
time. This section not only describes every potential dependecy, but also
provides information on relevant compilation flags for each of those.


.. note::
  See also :ref:`the CMake flags section <compilation_flags>` for more
  information on CMake flags.

Libraries that must be provided externally
==========================================
Required:
  * :ref:`BLAS<flags_blas>` and :ref:`LAPACK<flags_lapack>`
Not required but strongly recommended:
  * :ref:`MPI<flags_mpi>`
  * :ref:`ScaLAPACK<flags_scalapack>` (required for MPI)
  * :ref:`NetCDF<flags_netcdf>`
  * :ref:`FFTW<flags_fftw>`
Optional:
  * :ref:`OpenMP<flags_openmp>`
  * :ref:`libxc<flags_libxc>`
  * :ref:`ELPA<flags_elpa>`

Libraries fetched automatically by cmake
========================================

Core, required:
  * :ref:`xmlf90<flags_xmlf90>`
  * :ref:`libfdf<flags_fdf>`
  * :ref:`libpsml<flags_psml>`
  * :ref:`libgridxc<flags_gridxc>`

Optional:
  * :ref:`simple-DFTD3<flags_dftd3>`
  * :ref:`FLOOK<flags_flook>` (requires `readline <https://tiswww.case.edu/php/chet/readline/rltop.html>`_ package)

To ease the installation several of the packages are shipped in the Siesta
source tree. These will be fetched automatically by cmake, but they can also be
checked out by doing:

.. code-block:: shell

  git submodule update --init --recursive

If users do not have internet access on the compiling machine one must send the
sources by other means. To aid this procedure one may use the
`stage_submodules.sh` script to gather all sources for later uploading.

Ensure that the required packages are present in these environment variables:

- `CMAKE_PREFIX_PATH` variable
- `PKG_CONFIG_PATH` variable

For instance:
.. code-block:: shell

  CMAKE_PREFIX_PATH=/path/libxc/share/cmake:/path/libgridxc/share/cmake
  PKG_CONFIG_PATH=/path/libxc/lib/pkgconfig:/path/libgridxc/pkgconfig
  cmake ...

which ensures that CMake can search in the appropriate directories.
Alternatively one can put `CMAKE_PREFIX_PATH` as a CMake variable:

.. code-block:: shell

  cmake ... -DCMAKE_PREFIX_PATH=/path/libxc/share/cmake;/path/libgridxc/share/cmake

Note the different delimiters, `:` (Unix OS) vs. `;` (CMake list separator).


Flags related to external libraries
***********************************


Linear Algebra
==============

.. _flags_blas:

BLAS (required)
^^^^^^^^^^^^^^^
- `BLAS_LIBRARY=<name of library>|NONE` specifies the library name for linking.
  If `NONE` BLAS is implicitly linked through other libraries/flags or the
  compiler itself (e.g. Cray or for instance in OpenBLAS LAPACK can be
  implicitly contained and the BLAS library is not needed).
- `BLAS_LIBRARY_DIR=<path to library>` place where to find the library
  `BLAS_LIBRARY`
- `BLAS_LINKER_FLAG` flags to use when linking

Example:
.. code-block:: shell

   cmake ... -DBLAS_LIBRARY=blis -DBLAS_LIBRARY_DIR=/opt/blis/lib

.. _flags_lapack:

LAPACK (required)
^^^^^^^^^^^^^^^^^
- `LAPACK_LIBRARY=<name of library>|NONE` specifies the library name for
  linking. If `NONE` LAPACK is implicitly linked through other libraries/flags
  or the compiler itself (e.g. Cray).
- `LAPACK_LIBRARY_DIR=<path to library>` place where to find the library
  `LAPACK_LIBRARY`
- `LAPACK_LINKER_FLAG` flags to use when linking

Example:
.. code-block:: shell

  cmake ... -DLAPACK_LIBRARY=openblas -DLAPACK_LIBRARY_DIR=/opt/openblas/lib -DBLAS_LIBRARY=NONE

.. _flags_scalapack:

ScaLAPACK (optional, required for MPI support)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- `SCALAPACK_LIBRARY=<name of library>|NONE` specifies the library name for
  linking. If `NONE` ScaLAPACK is implicitly linked through other
  libraries/flags or the compiler itself.
- `SCALAPACK_LIBRARY_DIR=<path to library>` place where to find the library
  `SCALAPACK_LIBRARY`
- `SCALAPACK_LINKER_FLAG` flags to use when linking

Example:
.. code-block:: shell

  cmake ... -DSCALAPACK_LIBRARY="-lmkl=cluster" -DBLAS_LIBRARY=NONE -DLAPACK_LIBRARY=NONE


Parallelization
===============

.. _flags_mpi:

MPI (optional, highly recommended)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- `WITH_MPI=ON|OFF` to enable, disable support respectively.

MPI will defaulted to be `ON` when an MPI compiler is found.
Using MPI will forcefully require ScaLAPACK (see above).

.. _flags_openmp:

OpenMP (optional)
^^^^^^^^^^^^^^^^^
Enable threading support using OpenMP.

While only some parts of SIESTA may benefit from OpenMP parallelization, Both
TranSiesta/TBtrans can show performance gains and memory reductions when running
large systems.

- `WITH_OPENMP=OFF|ON` to disable, enable support respectively.
  By default it is `OFF`.

Users are recommended to test whether it makes sense for them to
utilize the threading support.

Be aware of `OMP_NUM_THREADS` and `OMP_PROC_BIND` environment variables
which may highly influence the performance gains.


Optional features
=================

.. _flags_libxc:

libxc (optional, recommended)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
libxc is an XC functional library implementing a very large variety of functionals.
libgridxc can leverage libxc and use the functionals from there.

- `WITH_LIBXC=ON|OFF` to enable, disable support respectively.
  Defaults to `ON` if the library can be found.
- `LIBXC_ROOT=<path to installation>` where libxc has been installed.
- `LIBXC_Fortran_INTERFACE=f03;f90` to search for a specific interface,
  defaults to both, but prefers the f03 interface. To only search for
  f90, do `-DLIBXC_Fortran_INTERFACE=f90`.

.. _flags_netcdf:

NetCDF (optional, highly recommended)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Enable writing NetCDF files for faster (parallel) IO and
also for easier post-processing utilities.


- `WITH_NETCDF=ON|OFF` to enable, disable support respectively.
  Defaults to `ON` if NetCDF can be found (i.e. specifying `NetCDF_PATH`
  should be enough).
- `NetCDF_ROOT|PATH=<path to installation>` to specificy the location
  of the NetCDF installation. Generally should be enough with this
  flag.
- `NetCDF_INCLUDE_DIR` to manually specify include directories
  for modules etc.


In conjunction with NetCDF there are supporter libraries shipped
with Siesta which are required for TBtrans.

- `WITH_NCDF=ON|OFF` enable NCDF support (a wrapper around NetCDF
  that makes it easier to work with).
  This is automatically detected.
  The default is sufficient.

- `WITH_NCDF_PARALLEL=ON|OFF` allow parallel IO through NCDF library.
  This is automatically detected.
  The default is sufficient.

.. _flags_elpa:

ELPA (optional, recommended)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

See `Config/cmake/Modules/FindCustomElpa.cmake` for details on
how to link against ELPA.

.. _flags_fftw:

FFTW (optional, recommended)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The FFTW library is only used in the Util/STM/ol-stm utility.

- `WITH_FFTW=ON|OFF` to enable, disable support respectively.
  Defaults to `ON` if the library can be found.

Flags for libraries fetched by CMake
************************************

SIESTA Core libraries
=====================

.. _flags_xmlf90:

xmlf90 (required)
^^^^^^^^^^^^^^^^^
Contained in the External/xmlf90 folder. Can be pre-installed, installed from
custom source directory, fetched at compile time.

- `XMLF90_FIND_METHOD=cmake/pkgconf/fetch/source`
  a CMake list of multiple ways to check for library existance, `cmake;fetch`
  will first search using CMake `find_package`, if that fails it will fetch it
  from the `XMLF90_GIT_REPOSITORY` variables.   `cmake` and `pkgconf` are
  generically implemented using package finders shipped with CMake. Ensure
  `CMAKE_PREFIX_PATH` and `PKG_CONFIG_PATH` are (prepended)/appended with the
  directories that should be searched. This value defaults to
  `SIESTA_FIND_METHOD`. `SIESTA_FIND_METHOD` defaults to `cmake;pkgconf;source;fetch`.
- `XMLF90_SOURCE_DIR` should point to a directory where the sources are present,
  either manually cloned on unpacked from a release archive. Applicable when
  `XMLF90_FIND_METHOD=source`
- `XMLF90_GIT_TAG` when `XMLF90_FIND_METHOD=fetch` this revision of the source
  will be checked out.
- `XMLF90_GIT_REPOSITORY` is the URL of the Git repository when cloning the
  sources. Is defaulted to the original development site, may be useful for
  testing clones with fixes/changes.


.. _flags_fdf:

libfdf (required)
^^^^^^^^^^^^^^^^^
Contained in the External/libfdf folder. Can be pre-installed, installed from
custom source directory, fetched at compile time.

- `LIBFDF_FIND_METHOD=cmake/pkgconf/fetch/source`
  a CMake list of multiple ways to check for library existance, `cmake;fetch`
  will first search using CMake `find_package`, if that fails it will fetch it
  from the `LIBFDF_GIT_REPOSITORY` variables `cmake` and `pkgconf` are
  generically implemented using package finders shipped with CMake. Ensure
  `CMAKE_PREFIX_PATH` and `PKG_CONFIG_PATH` are (prepended)/appended with the
  directories that should be searched. This value defaults to
  `SIESTA_FIND_METHOD`. `SIESTA_FIND_METHOD` defaults to `cmake;pkgconf;source;fetch`.
- `LIBFDF_SOURCE_DIR` should point to a directory where the sources are present,
  either manually cloned on unpacked from a release archive. Applicable when
  `LIBFDF_FIND_METHOD=source`
- `LIBFDF_GIT_TAG` when `LIBFDF_FIND_METHOD=fetch` this revision of the source
  will be checked out.
- `LIBFDF_GIT_REPOSITORY` is the URL of the Git repository when cloning the
  sources. Is defaulted to the original development site, may be useful for
  testing clones with fixes/changes.


libpsml (required)
^^^^^^^^^^^^^^^^^^

.. _flags_psml:

libpsml enables the reading of pseudopotential files in the `PSML` file format.
It thus enables re-use of PSML files from www.pseudo-dojo.org, amongst others.

- `LIBPSML_FIND_METHOD=cmake/pkgconf/fetch/source`
  a CMake list of multiple ways to check for library existance, `cmake;fetch`
  will first search using CMake `find_package`, if that fails it will fetch it
  from the `LIBPSML_GIT_REPOSITORY` variables `cmake` and `pkgconf` are
  generically implemented using package finders shipped with CMake. Ensure
  `CMAKE_PREFIX_PATH` and `PKG_CONFIG_PATH` are (prepended)/appended with the
  directories that should be searched. This value defaults to
  `SIESTA_FIND_METHOD`. `SIESTA_FIND_METHOD` defaults to `cmake;pkgconf;source;fetch`.
- `LIBPSML_SOURCE_DIR` should point to a directory where the sources are present,
  either manually cloned on unpacked from a release archive. Applicable when
  `LIBPSML_FIND_METHOD=source`
- `LIBPSML_GIT_TAG` when `LIBPSML_FIND_METHOD=fetch` this revision of the source
  will be checked out.
- `LIBPSML_GIT_REPOSITORY` is the URL of the Git repository when cloning the
  sources. Is defaulted to the original development site, may be useful for
  testing clones with fixes/changes or.

.. _flags_gridxc:

libgridxc (required)
^^^^^^^^^^^^^^^^^^^^
libgridxc enables the calculation of the XC functionals on the grid where
density is calculated. It can leverage the libxc library, which is also highly
recommended.

libgridxc depends on the `WITH_GRID_SP` flag which controls the precision of the
grid operations. It also depends on `WITH_MPI` for parallel support.

It has optional support for libxc which is highly recommended.

- `LIBGRIDXC_FIND_METHOD=cmake/pkgconf/fetch/source`
  a CMake list of multiple ways to check for library existance, `cmake;fetch`
  will first search using CMake `find_package`, if that fails it will fetch it
  from the `LIBGRIDXC_GIT_REPOSITORY` variables. `cmake` and `pkgconf` are
  generically implemented using package finders shipped with CMake. Ensure
  `CMAKE_PREFIX_PATH` and `PKG_CONFIG_PATH` are (prepended)/appended with the
  directories that should be searched. This value defaults to
  `SIESTA_FIND_METHOD`. `SIESTA_FIND_METHOD` defaults to `cmake;pkgconf;source;fetch`.
- `LIBGRIDXC_SOURCE_DIR` should point to a directory where the sources are
  present, ither manually cloned on unpacked from a release archive.
  Applicable when `LIBGRIDXC_FIND_METHOD=source`
- `LIBGRIDXC_GIT_TAG` when `LIBGRIDXC_FIND_METHOD=fetch` this revision of the
  source will be checked out.
- `LIBGRIDXC_GIT_REPOSITORY` is the URL of the Git repository when cloning the
  sources. Is defaulted to the original development site, may be useful for
  testing clones with fixes/changes.


Optional features
=================

.. _flags_dftd3:

simple-DFTD3 (optional)
^^^^^^^^^^^^^^^^^^^^^^^
Add support for DFTD3 dispersion corrections as suggested by Grimme et. al.

- `WITH_DFTD3=ON|OFF` to enable, disable support respectively. Defaults to `ON`
  if the `./External/DFTD3/` directory contains directories with the appropriate
  sources.
- `S-DFTD3_FIND_METHOD=cmake/pkgconf/fetch/source` a CMake list of multiple ways
  to check for library existance, `cmake;fetch` will first search using CMake
  `find_package`, if that fails it will fetch it from the `S-DFTD3_GIT_REPOSITORY`
  variables. `cmake` and `pkgconf` are generically implemented using package
  finders shipped with CMake. Ensure `CMAKE_PREFIX_PATH` and `PKG_CONFIG_PATH`
  are (prepended)/appended with the  directories that should be searched.
  This value defaults to `SIESTA_FIND_METHOD`. `SIESTA_FIND_METHOD` defaults to
  `cmake;pkgconf;source;fetch`.
- `S-DFTD3_SOURCE_DIR` should point to a directory where the sources are
  present, either manually cloned on unpacked from a release archive.
  Applicable when `S-DFTD3_FIND_METHOD=source`.
- `S-DFTD3_GIT_TAG` when `S-DFTD3_FIND_METHOD=fetch` this
  revision of the source will be checked out.
- `S-DFTD3_GIT_REPOSITORY` is the URL of the Git repository when cloning the
  sources. Is defaulted to the original development site, may be useful for
  testing clones with fixes/changes.


mctc, mstore, test-drive, toml-f
""""""""""""""""""""""""""""""""
These packages are dependencies of the `simple-DFTD3` library. Generally one
need not change these unless one changes the `S-DFTD3_*` flags in which case
dependencies may require manual changes.

Here are flags for each of these sub-dependencies.

- `MCTC-LIB_FIND_METHOD=cmake/pkgconf/fetch/source` a CMake list of multiple
  ways to check for library existance, `cmake;fetch` will first search using
  CMake `find_package`, if that fails it will fetch it from the
  `MCTC-LIB_GIT_REPOSITORY` variables. `cmake` and `pkgconf` are generically
  implemented using package finders shipped with CMake. Ensure `CMAKE_PREFIX_PATH`
  and `PKG_CONFIG_PATH` are (prepended)/appended with the directories that
  should be searched. This value defaults to `SIESTA_FIND_METHOD`.
  `SIESTA_FIND_METHOD` defaults to `cmake;pkgconf;source;fetch`.
- `MCTC-LIB_SOURCE_DIR` should point to a directory where the sources are
  present, either manually cloned on unpacked from a release archive.
  Applicable when `MCTC-LIB_FIND_METHOD=source`.
- `MCTC-LIB_GIT_TAG` when `MCTC-LIB_FIND_METHOD=fetch` this
  revision of the source will be checked out.
- `MCTC-LIB_GIT_REPOSITORY` is the URL of the Git repository when cloning the
  sources. Is defaulted to the original development site, may be
  useful for testing clones with fixes/changes.

- `MSTORE_FIND_METHOD=cmake/pkgconf/fetch/source`
  a CMake list of multiple ways to check for library existance, `cmake;fetch`
  will first search using CMake `find_package`, if that fails it will fetch it
  from the `MSTORE_GIT_REPOSITORY` variables. `cmake` and `pkgconf` are
  generically implemented using package finders shipped with CMake. Ensure
  `CMAKE_PREFIX_PATH` and `PKG_CONFIG_PATH` are (prepended)/appended with the
  directories that should be searched. This value defaults to
  `SIESTA_FIND_METHOD`. `SIESTA_FIND_METHOD` defaults to `cmake;pkgconf;source;fetch`.
- `MSTORE_SOURCE_DIR` should point to a directory where the sources are present,
  either manually cloned on unpacked from a release archive.
  Applicable when `MSTORE_FIND_METHOD=source`.
- `MSTORE_GIT_TAG` when `MSTORE_FIND_METHOD=fetch` this revision of the source
  will be checked out.
- `MSTORE_GIT_REPOSITORY` is the URL of the Git repository when cloning the
  sources. Is defaulted to the original development site, may be useful for
  testing clones with fixes/changes.

- `TEST-DRIVE_FIND_METHOD=cmake/pkgconf/fetch/source`
  a CMake list of multiple ways to check for library existance, `cmake;fetch`
  will first search using CMake `find_package`, if that fails it will fetch it
  from the `TEST-DRIVE_GIT_REPOSITORY` variables. `cmake` and `pkgconf` are
  generically implemented using package finders shipped with CMake. Ensure
  `CMAKE_PREFIX_PATH` and `PKG_CONFIG_PATH` are (prepended)/appended with the
  directories that should be searched. This value defaults to `SIESTA_FIND_METHOD`.
  `SIESTA_FIND_METHOD` defaults to `cmake;pkgconf;source;fetch`.
- `TEST-DRIVE_SOURCE_DIR` should point to a directory where the sources are
  present, ither manually cloned on unpacked from a release archive.
  Applicable when `TEST-DRIVE_FIND_METHOD=source`.
- `TEST-DRIVE_GIT_TAG` when `TEST-DRIVE_FIND_METHOD=fetch` this
  revision of the source will be checked out.
- `TEST-DRIVE_GIT_REPOSITORY` is the URL of the Git repository when cloning the
  sources. Is defaulted to the original development site, may be useful for
  testing clones with fixes/changes.

- `TOML-F_FIND_METHOD=cmake/pkgconf/fetch/source`
  a CMake list of multiple ways to check for library existance, `cmake;fetch`
  will first search using CMake `find_package`, if that fails it will fetch it
  from the `TOML-F_GIT_REPOSITORY` variables. `cmake` and `pkgconf` are
  generically implemented using package finders shipped with CMake. Ensure
  `CMAKE_PREFIX_PATH` and `PKG_CONFIG_PATH` are (prepended)/appended with the
  directories that should be searched. This value defaults to `SIESTA_FIND_METHOD`.
  `SIESTA_FIND_METHOD` defaults to `cmake;pkgconf;source;fetch`.
- `TOML-F_SOURCE_DIR` should point to a directory where the sources are present,
  either manually cloned on unpacked from a release archive.
  Applicable when `TOML-F_FIND_METHOD=source`.
- `TOML-F_GIT_TAG` when `TOML-F_FIND_METHOD=fetch` this
  revision of the source will be checked out.
- `TOML-F_GIT_REPOSITORY` is the URL of the Git repository when cloning the
  sources. Is defaulted to the original development site, may be useful for
  testing clones with fixes/changes.

.. _flags_flook:

FLOOK (optional)
^^^^^^^^^^^^^^^^
A library for interacting with the internal Siesta variables on the fly and/or
create custom molecular dynamics trajectories. It exposes the Lua language for
scripting capabilities.

- `WITH_FLOOK=ON|OFF` to enable, disable support respectively.
  Defaults to `ON` if found.

