Know issues when building SIESTA
################################

SIESTA does not recognize the compiler I want to use
----------------------------------------------------
Different environments might result in SIESTA not detecting automatically your
compiler and defaulting to the GNU compiler, getting the following message::

    -- The Fortran compiler identification is GNU 9.4.0
    -- The C compiler identification is GNU 9.4.0

If your intention was to use other compilers, such as Intel or Cray, you can
override the compiler in cmake using the ``CC``, ``FC`` and ``MPIFC``
environment variables at the configuration step. For example::

    CC=icc FC=ifort MPIFC=mpiifort cmake -S. -B _build ...[Your Options Here]...

In this case, this will force cmake to use the Intel compiler (if available).


SIESTA does not recognize GNU ScaLAPACK
---------------------------------------
In some Linux-native versions of the ScaLAPACK distribution, CMake detection
might fail with the following message::

    CMake Warning at CMakeLists.txt:63 (message):
    MPI is found, but ScaLAPACK library cannot be found (or compiled against).
    If parallel support is required please supply the ScaLAPACK library with
    appropriate flags:
    -DSCALAPACK_LIBRARY=<lib>

In order to fix this, the cmake variable -DSCALAPACK_LIBRARY must be explicitly set to
``-DSCALAPACK_LIBRARY="-lscalapack-openmpi"`` (or the appropriate ScaLAPACK version in
your system)::

    cmake -S. -B _build ...[Your Options Here]... -DSCALAPACK_LIBRARY="-lscalapack-openmpi"

SIESTA does not recognize Intel MKL libraries
---------------------------------------------
When using Intel-provided compilers and libraries (such as OneAPI), you might need
to set the name for linear algebra libraries manually::

    cmake -S. -B _build ...[Your Options Here]... \
    -DLAPACK_LIBRARY="-lmkl_intel_lp64 -lmkl_sequential -lmkl_core" \
    -DSCALAPACK_LIBRARY="-lmkl_scalapack_lp64 -lmkl_blacs_intelmpi_lp64 -lmkl_intel_lp64 -lmkl_sequential -lmkl_core"

Issues with Cray compilers
--------------------------
There are few known issues when compiling SIESTA with Cray compilers.

* Compilation with debug information "-g" fails for Cray Compiler versions lower
  than 14.0.3.

* For Cray versions 15.0 or higher available on certain systems, it might be
  mandatory to manually add a compiler flag for OpenMP compilations. For example::

    cmake -S. -B _build ...[Your Options Here]... -DFortran_FLAGS="-fopenmp"


Missing readline (FLOOK)
------------------------

CMake configuration may fail with an error similar to this one::

  CMake Error at External/Lua-Engine/CMakeLists.txt:49 (find_library):
    Could not find _flook_lib_readline using the following names: readline
  Call Stack (most recent call first):
    External/Lua-Engine/CMakeLists.txt:229 (flook_add_dependencies)

This happens when you are missing the
`readline package   <https://tiswww.case.edu/php/chet/readline/rltop.html>`_
in your system. You can either disable FLOOK for your compilation if you don't
need it (``-DWITH_FLOOK=off``), or install readline. In modern Debian-based
distributions such as Ubuntu this is as easy as::

    apt install readline-common