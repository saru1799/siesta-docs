Building SIESTA with CMake
##########################

.. _building_with_cmake:

In this section, we describe how to compile SIESTA environment.
We thus assume that you have already completed the steps explained
in :doc:`build-prep-env`.

There are 2 steps involved in the manual building of SIESTA:

- Download and uncompress the source code.
- Configure and compile the build.

Siesta requires CMake >= 3.17, and (if used) the ninja (>=1.10) backend.
Both cmake and ninja can be installed easily in most systems, in
particular with conda or pip.



Building SIESTA
***************

Quick and go
============

The most basic compilation of Siesta can be done simply by:

.. code-block:: shell

  cmake -S. -B_build -DCMAKE_INSTALL_PREFIX=/path/to/installation
  cmake --build _build -j 4
  cmake --install _build

If all required dependencies are found this will succeed, otherwise
follow below instructions.


Building in parallel (recommended!)
===================================

To build in parallel simply add these flags:

.. code-block:: shell

  cmake ... -j 4

to build using 4 processes.


.. _compilation_flags:

Feature and dependecy flags
***************************
.. note::

  See also :ref:`dependency-releated flags <install_dependencies>`.

Features:

- *-DWITH_MPI=ON|OFF*

  Enable MPI support (ON by default).
- *-DWITH_OPENMP=OFF|ON*

  Enable OpenMP support (OFF by default).
- *-DWITH_DFTD3=ON|OFF*

  Enable DFT-D3 corrections (ON by default).
- *-DWITH_FLOOK=ON|OFF*

  Enable FLOOK/LUA support. Defaults to `ON` if LUA and readline are found.
- *-DWITH_NETCDF=ON|OFF*

  Enable NetCDF support. Defaults to `ON` if NetCDF can be found.
- *-DWITH_LIBXC=ON|OFF*

  Enable libXC support. Defaults to `ON` if the library can be found.
- *-DWITH_FFTW=ON|OFF*

  Enable FFTW support. Defaults to `ON` if the library can be found.

Linear algebra libraries:

- *-DBLAS_LIBRARY=<name of library>|NONE*

  Specifies the library name for linking. If `NONE` BLAS is implicitly linked
  through other libraries/flags or the compiler itself.
- *-DLAPACK_LIBRARY=<name of library>|NONE*

  Specifies the library name for linking. If `NONE` LAPACK is implicitly linked
  through other libraries/flags or the compiler itself.
- *-DSCALAPACK_LIBRARY=<name of library>|NONE*

  Specifies the library name for linking. If `NONE` ScaLAPACK is implicitly
  linked through other libraries/flags or the compiler itself.

Compiler flags
**************
Compilation flags are generally managed through the environment
variables (NOT CMake variables).

- `FC` for specifying the fortran compiler
- `FFLAGS` for specifying the compilation flags

An invocation might be:
.. code-block:: shell

  FC=gfortran FFLAGS='-O3 -march=native' cmake ...

Alternatively, the flags can be supplied on the command line:

.. code-block:: shell

  cmake -DFortran_FLAGS=-Os -DC_FLAGS=-Os

This enables fine tuning of the compiler flags.

.. note::
  Customarily, CMake uses the `CMAKE_<LANG>_FLAGS`.
  These may still be used, but the shorter, simpler flags
  allows less typing and faster proto-typing.

Siesta's infrastructure also allows the usage of toolchain files.

This can either be set in 2 different ways:

.. code-block:: shell

  # Use default toolchain files located in Config/cmake/toolchains
  cmake ... -DSIESTA_TOOLCHAIN=gnu
  # or a full path (for local edited files)
  cmake ... -DSIESTA_TOOLCHAIN=/path/to/toolchain/file/gnu.cmake

  # Direct usage of the toolchain file
  cmake ... -C Config/cmake/toolchains/gnu.cmake
  # or equivalently
  cmake ... -DCMAKE_TOOLCHAIN_FILE=Config/cmake/toolchains/gnu.cmake


When using `SIESTA_TOOLCHAIN` one can use multiple toolchains.
This can be valuable for overwriting or adding variables from various
toolchains. Mainly useful for developers.

.. code-block:: shell

  cmake -DSIESTA_TOOLCHAIN=gnu;local ...

to use `./Config/cmake/toolchains/gnu.cmake` and `./local.cmake`.


These toolchain files may be used to default your variables and caching of the flags.

Currently the default toolchain will be decided with:
  - GNU compilers will use the `Config/cmake/toolchains/gnu.cmake` toolchain file.
  - Intel (and the newer Intel LLVM backend) compilers will use the `Config/cmake/toolchains/intel.cmake` toolchain file.
  - Otherwise a _generic_ toolchain file will be used, which uses the default CMake variables.


To gain complete control of the compiler flags (without adding the toolchain
ones) you will have to select the `none` toolchain and set the flags.
.. code-block:: shell

  cmake -DSIESTA_TOOLCHAIN=none -DFortran_FLAGS="-Os -Dasheusatoehu"

A custom toolchain may contain any setting of variables. They can
be thought of as an `arch.make` file with default parameters.
Parameters that exists in a toolchain file can be overwriting on
the command-line with `cmake -D<VAR>=<VALUE>` for temporary
changing its value.

Further Options
===============

Siesta provides a set of options that controls the capabilities
or some intricate feature of Siesta. The generic Siesta executable
should be sufficient for most, but some may need different details.

- `WITH_GRID_SP=OFF|ON` use single-precision grid operations (`ON`).
  Can greatly reduce the memory requirements for large mesh-cutoffs
  and/or large unit-cells. At the expense of some precision.
  The default is to use double precision `-DWITH_GRID_SP=OFF`


Build types
***********

CMake compilation infrastructure utilizes a build-type to determine the
flags used.

These build-types are primarily used for experienced users, the default
build type (`Release`) should be sufficient for most (if not all users).

A specific build-type can be enabled with:

.. code-block:: shell

  cmake -DCMAKE_BUILD_TYPE=Debug

Currently the default Siesta toolchain files allows these different
build types:

- `Release`: the default and recommended build type, it uses a high optimization
  level without sacrifycing accuracy.
- `Debug`: used for debugging Siesta, or if there are runs that shows problems
  this build-type may be useful.
  *Bug reports* should use this build
- `Check`: used for debug + checking code execution runs, primarily
  useful for developers; equally good for bug-reports.
- `RelWithDebInfo`: a release mode with debug mode.
- `MinSizeRel`: optimizes the executables for minimum size (`-Os`)

One can specify different compiler flags for different build types
to more easily switch between them, for instance:

.. code-block:: shell

  cmake -DFortran_FLAGS=-Os -DFortran_FLAGS_DEBUG=-g -DCMAKE_BUILD_TYPE=Debug

will use the `Fortran_FLAGS_DEBUG` flags while omitting the `Fortran_FLAGS`.
This allows toolchain files to be self-contained and contain multiple
user-configurations.

The currently supported build-types in the shipped toolchain files are:
- `Fortran_FLAGS`
- `Fortran_FLAGS_RELEASE`
- `Fortran_FLAGS_DEBUG`
- `Fortran_FLAGS_CHECK`
- `Fortran_FLAGS_RELWITHDEBINFO`
- `Fortran_FLAGS_MINSIZEREL`

Testing
*******

CMake integrates a testing framework, which can be run via ctest when inside the
_build directory:

.. code-block:: shell

  cd _build
  ctest <options>

If the required external libraries have been compiled as part of the current
CMake invocation, installation tests for them will also be executed. To run
ctest with output verification, you can instead run ctest using the VERIFY_TESTS
environment variable:

.. code-block:: shell

  VERIFY_TESTS=1 ctest <options>

Developers
**********

Developers are suggested to create custom toolchain files with the appropriate
compiler flags and linker flags to sustain a quick and easy turn-around for
the compilation procedure.


.. note::

  Bash scripts are notorious for omitting quotation marks when passing
  variables to CMake. For instance, a small script like this will fail
  due to the quotation  marks being disconnected when passed as arguments
  to the `cmake` executable:

  .. code-block:: shell

    opts="-DFortran_FLAGS='-Os -g'
    cmake $opts

  Full control is easier to gain by using custom toolchain files.