.. _installation:

Installing SIESTA
=================

.. _how-to-build-siesta:

.. toctree::
    :maxdepth: 1

    quick-install
    conda
    spack

Building Siesta from source
---------------------------
.. toctree::
    :maxdepth: 1

    build-overview
    build-prep-env
    dependencies
    build-manually
    build-issues
    legacy-build

Other
-----
.. toctree::
    :maxdepth: 1

    flos


